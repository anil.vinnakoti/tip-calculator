import React, { Component } from 'react';

class Form extends Component {
    
  render() {
    const {number, bill} = this.props;
    // this.formRef = React.createRef()
    const styles = {
      display:'flex',
      flexDirection:'column'
    }

    return (
        <form >
            <div style={styles}>
                <label >{this.props.content}</label>
                <input type='text' value={number} placeholder={this.props.placeHolder} ref = {this.props.refValue} onChange={this.props.handler}/>
            </div>
        </form>
    )
  }
}

export default Form;
