import React, { Component } from 'react';
import Person from './person';
import Reset from './resetBtn';
class Output extends Component {

    render() { 

        const styles={
            width:'49%',
            padding:'1.5rem',
            borderRadius:'0.6rem',
            display:'flex',
            flexDirection:'column',
            backgroundColor: 'hsl(183, 100%, 15%)',
            color:'hsl(0, 0%, 100%)',
            fontSize:'0.9rem'
        }

        return (
            <div style={styles}>
                <Person content = 'Tip Amount' display={this.props.tip}/>
                <Person content = 'Total' display = {this.props.bill}/>
                <Reset reset = {this.props.reset}/>
            </div>
        );
    }
}
 
export default Output;