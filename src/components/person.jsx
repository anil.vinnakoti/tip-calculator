import React, { Component } from 'react';

class Person extends Component {
    render() { 

        const stylesMain = {
            display:'flex',
            justifyContent:'space-between',
            marginBottom:'20px',
          }

          const innerStyles = {
              display:'flex',
              flexDirection:'column'
          }
          const colorForTitles ={
            color:'hsl(172, 67%, 45%)'
          }

        return (
            <div style={stylesMain}>
                <div style={innerStyles}>
                    <span>{this.props.content}</span>
                    <span style={colorForTitles}>/ person</span>
                </div>

                <div>
                    ${this.props.display}
                </div>
            </div>
        );
    }
}
 
export default Person;