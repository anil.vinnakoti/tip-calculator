import React, { Component } from 'react';
import Tip from './tip';
import Form from './form';

class Input extends Component {
    render() { 
        const {handleBillForm, tipBtnHandler, customFormHandler, peopleHandlerForn} = this.props;
        const styles = {
            display:'flex',
            flexDirection:'column',
            justifyContent:'space-around',
            width:'49%'
        }

        return (
            <div style={styles}>
                <Form refValue = {this.props.refValue} content='Bill Amount' handler = {handleBillForm}/>
                <Tip tipHandler =  {tipBtnHandler} refValue = {this.props.refValue}  customHandler={customFormHandler}/>
                <Form refValue = {this.props.refValue} content='Number of People' handler = {peopleHandlerForn}/>
            </div>
        );
    }
}
 
export default Input;