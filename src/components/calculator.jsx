import React, { Component } from 'react';
import Input from './input';
import Output from './output';

const initialState = {
    bill: 0,
    percentage: 0,
    people: 1,
    tipPerPerson:0,
    billPerPerson:0
};


class Calculator extends Component {
    constructor() {
        super()
        this.state = {
            bill: 0,
            percentage: 0,
            people: 1,
            tipPerPerson:0,
            billPerPerson:0
        }
      }
  
      handleBillForm = event =>{
          this.setState({
              bill : parseFloat(event.target.value)
          }, () => this.calculate())
      }
  
      tipBtnHandler = event =>{
          this.setState({
              percentage: parseFloat(event.target.textContent)
          }, () => this.calculate())
      }
  
      customFormHandler = event =>{
          this.setState({
              percentage: parseFloat(event.target.value)
          }, () => this.calculate())

      }
  
      peopleHandlerForn = event =>{
          this.setState({
              people: parseFloat(event.target.value)
          }, () => this.calculate())
      }
  
      calculate(){
        const {bill, percentage, people} = this.state

        // if(bill >=0 && bill !== '')

        let tip = ((bill*percentage)/100)/people;
        let billPerson = ((bill/people)+tip)
        this.setState({
            tipPerPerson : tip,
            billPerPerson :billPerson 
        })

        //using component did update
        // this.tipPerPerson = ((this.state.bill*this.state.percentage)/100)/this.state.people;
        // this.billPerPerson = ((this.state.bill/this.state.people)+this.tipPerPerson)
        
      }

      resetBtnHandler = () => {

          this.setState(initialState, () => this.calculate())
      }



    render() { 

        const styles = {
            borderRadius: '1rem',
            width: '50%',
            padding: '2rem',
            display: 'flex',
            justifyContent: 'space-between',
            backgroundColor: 'hsl(0, 0%, 100%'
        }

        return (
            <div style={styles}>
               <Input 
               refValue = {this.formRef}
               handleBillForm = {this.handleBillForm}
               tipBtnHandler = {this.tipBtnHandler}
               customFormHandler = {this.customFormHandler}
               peopleHandlerForn = {this.peopleHandlerForn}
                />
               <Output tip = {this.state.tipPerPerson} bill = {this.state.billPerPerson} reset={this.resetBtnHandler}/> 

               {/* for componentDidUpdate method */}
               {/* <Output tip = {this.tipPerPerson} bill = {this.billPerPerson}/>  */}
            </div>
        );
    }
}
 
export default Calculator;