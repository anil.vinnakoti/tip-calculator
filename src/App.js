import './App.css';
import Calculator from './components/calculator';

function App() {

  const styles = {
    height:'100vh',
    display: 'flex',
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor:'hsl(185, 41%, 84%'
  }

  return (
    <div style={styles} className="App">
      <Calculator/>
    </div>
  );
}

export default App;
